Ansible-PuppetEnterprise
========================

## The Goal: Deploy Puppet Enterprise using Ansible

_Using a multi server deployment to seperate the PuppetDB & PupConsole from the PuppetMaster_

## Variables

Go here:

```
group_vars/all
```

With this format:

```
variable: value
```

_Default settings in the answer file are not using variables for simplicity._

## Common Tasks

  * Build and deploy a common /etc/hosts file for each server
  * Create a Puppet install directory in /root/puppetinstall
  * Create a Puppet installer log file
  * Download the PE installer tarball
  * Extract the tarball to the installer directory
  

## On each server

  * Build a Puppet installer answers file using a template
  * Run the PE installer using the answers file
  * Log the installer output to the log file using 'tee'
  * **Does Not** do the first 'puppet agent' run so the certs are not exchanged with the Puppet Master - this is a manual step
  
## ToDo

  * Use Ansible Vault to encrypt all passwords
  * Create a Uninstall playbook

